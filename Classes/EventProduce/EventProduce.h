#pragma once
#include "Events.h"
#include "../single.h"
#include <vector>
using namespace std;

#define SAFE_DEL(x) {if(x){delete x;x=0;}}

typedef vector<IEvent*> vE;
typedef vE::iterator vEIter;

class IEventListen
{
public:
	virtual void OnEvent(IEvent*pEvent) = 0;
public:
	void SetEnable(bool enable){ m_bEnable = enable; }
	bool IsEnable(){ return m_bEnable; }
private:
	bool m_bEnable;
};

typedef vector<IEventListen*> vEL;
typedef vEL::iterator vELIter;

class CEventProduce
{
public:
	CEventProduce(){}
	~CEventProduce(){}

public:
	void RegListen(IEventListen * pListen){ pListen->SetEnable(true); m_vel.push_back(pListen); }
	void UnRegListen(IEventListen * pListen){pListen->SetEnable(false);}

public:
	void FireEvent(IEvent*pEvent){		
		for (vELIter it = m_vel.begin(); it != m_vel.end(); ++it)
		{
			if (!(*it)->IsEnable())
			{
				continue;
			}
			(*it)->OnEvent(pEvent);
		}
		SAFE_DEL(pEvent);
	}


private:
	vEL m_vel;
	vEL m_Unvel;	//ȡ��ע�����,��ֹ��ͻ
};

#define EVENT_PRODUCE singleton_t<CEventProduce>::instance()