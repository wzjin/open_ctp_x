#include "LoginScene.h"
USING_NS_CC;

#include "./ctpWrapper/FACTPWrapper.h"
#include "QuoteScene.h"

using namespace cocostudio::timeline;

Scene* LoginScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = LoginScene::create();

    // add layer as a child to scene
    scene->addChild(layer);
	
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LoginScene::init()
{    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
	
	return true;
}

void LoginScene::onEnterTransitionDidFinish()
{
	EVENT_PRODUCE->RegListen(this);

	m_csbNode = CSLoader::createNode("LoginScene.csb");

	addChild(m_csbNode);

	Button * pBtnLogin = dynamic_cast<Button*>(m_csbNode->getChildByName("Button_Login"));
	pBtnLogin->addTouchEventListener(this, toucheventselector(LoginScene::Option));

#if 0
	Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
	pTxt->setTextAreaSize(Size(600, 33));
#else
	;
#endif

}

void LoginScene::Option(Object* pSender,TouchEventType type)
{
#if 0
	Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif

	switch (type)
	{
	case TOUCH_EVENT_ENDED:
		//_Test();
		pTxt->setText("wait for ctp connect...");
		CTP_WRAP->CreateQuote();
		CTP_WRAP->CreateTrade();
		break;
	default:
		break;
	}

}

void LoginScene::_Test()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#if defined(COCOS2D_DEBUG)
	auto quoteScene = QuoteScene::createScene();
	Director::getInstance()->replaceScene(quoteScene);

#endif
#endif
}

void LoginScene::OnEvent(IEvent*pEvent)
{
	switch (pEvent->m_id)
	{
		/* TEST */
	case EVENT_CFG_TEST_1:
	{
#if 0
						   Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
						   pTxt->setText("[Test] info-1-!");
#else
							 TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-1-!");
							 
#endif

	}
		break;
	case EVENT_CFG_TEST_2:
	{
#if 0
							 Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-2-!");
#else
							 TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-2-!");

#endif

	}
		break;
	case EVENT_CFG_TEST_3:
	{
#if 0
							 Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-3-!");
#else
							 TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-3-!");

#endif

	}
		break;
	case EVENT_CFG_TEST_4:
	{
#if 0
							 Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-4-!");
#else
							 TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-4-!");

#endif

	}
		break;
	case EVENT_CFG_TEST_5:
	{
#if 0
							 Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-5-!");
#else
							 TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
							 pTxt->setText("[Test] info-5-!");

#endif

	}
		break;
		/*SO */
	case EVENT_SO_LOAD_ERR:
	{

							  EventSo * pSo = (EventSo*)pEvent;
#if 0
							  Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
							  TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif
							  std::string sz;
							  sz.append("so err:");
							  sz.append(pSo->errCode);
							  pTxt->setText(sz.c_str());

	}
		break;

		/* CFG */
	case EVENT_CFG_LOAD_ERR:
	{
							   EventCfgPath * pSo = (EventCfgPath*)pEvent;
#if 0
							   Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif
							   std::string sz;
							   sz.append("cfg err:");
							   sz.append(pSo->errCode);
							   pTxt->setText(sz.c_str());

	}
		break;

		/* CTP */
	case EVENT_CTP_CREATE_ERR:
	{
								 //connected
#if 0
								 Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif
								 pTxt->setString("err:ctp create err!");

	}
		break;
	case EVENT_CTP_CONNECTED:
	{
								//connected
#if 0
								Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif
								pTxt->setString("ctp server connectded !");

	}
		break;
	case EVENT_CTP_QUOTE_LOGIN:
	{
								  EVENT_PRODUCE->UnRegListen(this);
								  //scene�л�
								  //QuoteScene
								  auto quoteScene = QuoteScene::createScene();
								  //Director::getInstance()->replaceScene(quoteScene);
								  Director::getInstance()->pushScene(quoteScene);
	}
		break;
	case EVENT_CTP_DISCONNECTED:
	{
								   //disconnected
#if 0
								   Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif
								   pTxt->setText("err:ctp server disconnectded !");
	}
		break;
	case EVENT_CTP_EXIST:
	{
								//exist
#if 0
								Text *pTxt = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_CTP_STATUS_Value"));
#else
TextBMFont *pTxt = dynamic_cast<TextBMFont*>(m_csbNode->getChildByName("BitmapFontLabel_CTP_STATUS_Value"));
#endif
								pTxt->setText("warn:ctp already exist!");
	}
		break;
	default:
		break;
	}
}