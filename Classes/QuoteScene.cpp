#include "QuoteScene.h"
USING_NS_CC;

#include "./ctpWrapper/FACTPWrapper.h"

using namespace cocostudio::timeline;

#define TXT_W	20	//100
#define TXT_H	33	//33
#define CTP_STR_LEN	128

void QuoteItem::Create()
{
	m_pLayout = Layout::create();
	m_pLayout->setContentSize(cocos2d::Size(860, 60));

#if defined(USE_TXT)
	auto rootNode = CSLoader::createNode("InsItemNode.csb");
	/*txt s*/
	Layout * pLayout = dynamic_cast<Layout*>(rootNode->getChildByName("Panel_Item"));
	m_pTxtName = dynamic_cast<Text*>(pLayout->getChildByName("Text_Name"));
	m_pTxtAsk1 = dynamic_cast<Text*>(pLayout->getChildByName("Text_Ask1"));
	m_pTxtBid1 = dynamic_cast<Text*>(pLayout->getChildByName("Text_Bid1"));
	m_pTxtAskVol1 = dynamic_cast<Text*>(pLayout->getChildByName("Text_AskVol1"));
	m_pTxtBidVol1 = dynamic_cast<Text*>(pLayout->getChildByName("Text_BidVol1"));
#elif defined(USE_TXTBMFONT)
	auto rootNode = CSLoader::createNode("InsItemNode2.csb");
	/*txt s*/
	m_pTxtName = dynamic_cast<TextBMFont*>(rootNode->getChildByName("BitmapFontLabel_Name"));
	m_pTxtAsk1 = dynamic_cast<TextBMFont*>(rootNode->getChildByName("BitmapFontLabel_Ask1"));
	m_pTxtBid1 = dynamic_cast<TextBMFont*>(rootNode->getChildByName("BitmapFontLabel_Bid1"));
	m_pTxtAskVol1 = dynamic_cast<TextBMFont*>(rootNode->getChildByName("BitmapFontLabel_AskVol1"));
	m_pTxtBidVol1 = dynamic_cast<TextBMFont*>(rootNode->getChildByName("BitmapFontLabel_BidVol1"));

#endif

	//m_pTxtName->ignoreContentAdaptWithSize(false);
	//m_pTxtAsk1->ignoreContentAdaptWithSize(false);
	//m_pTxtBid1->ignoreContentAdaptWithSize(false);
	//m_pTxtAskVol1->ignoreContentAdaptWithSize(false);
	//m_pTxtBidVol1->ignoreContentAdaptWithSize(false);

	//m_pTxtName->setTextAreaSize(Size(TXT_W, TXT_H));
	//m_pTxtAsk1->setTextAreaSize(Size(TXT_W, TXT_H));
	//m_pTxtBid1->setTextAreaSize(Size(TXT_W, TXT_H));
	//m_pTxtAskVol1->setTextAreaSize(Size(TXT_W, TXT_H));
	//m_pTxtBidVol1->setTextAreaSize(Size(TXT_W, TXT_H));

	//char * sz = "123456789abcdefghijklmnopqrstuvwxyz"; 
	char *sz = "00";
	m_pTxtName->setText(sz);
	m_pTxtAsk1->setText(sz);
	m_pTxtBid1->setText(sz);
	m_pTxtAskVol1->setText(sz);
	m_pTxtBidVol1->setText(sz);

	m_pLayout->addChild(rootNode);
}

void QuoteItem::Update(CThostFtdcDepthMarketDataField*pData)
{
	if (strcmp(m_szIns,pData->InstrumentID)!=0)
		return;

	if (!m_pTxtName)
		return;

	m_pTxtName->setText(pData->InstrumentID);

	char sz[CTP_STR_LEN] = { 0 };
	sprintf(sz, "%.1f", pData->AskPrice1);
	m_pTxtAsk1->setText(sz);

	sprintf(sz, "%.1f", pData->BidPrice1);
	m_pTxtBid1->setText(sz);

	sprintf(sz, "%d", pData->AskVolume1);
	m_pTxtAskVol1->setText(sz);

	sprintf(sz, "%d", pData->BidVolume1);
	m_pTxtBidVol1->setText(sz);
}

void QuoteItemManager::Update(CThostFtdcDepthMarketDataField * pData)
{
	for (int i = 0; i < m_vQuoteItems.size();i++)
	{
		m_vQuoteItems[i]->Update(pData);
	}
}

//////////////////////////////////////////////////////////////////////////

Scene* QuoteScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = QuoteScene::create();

    // add layer as a child to scene
    scene->addChild(layer);
	
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool QuoteScene::init()
{    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
		
	return true;
}

void QuoteScene::onEnterTransitionDidFinish()
{
	EVENT_PRODUCE->RegListen(this);

	m_csbNode = CSLoader::createNode("QuoteScene.csb");

	Layout * pLayoutTop = dynamic_cast<Layout*>(m_csbNode->getChildByName("Panel_Top"));
	m_pBtnExit = dynamic_cast<Button*>(pLayoutTop->getChildByName("Button_Exit"));
	m_pBtnExit->addTouchEventListener(this, toucheventselector(QuoteScene::Option));
	m_pBtnSub = dynamic_cast<Button*>(m_csbNode->getChildByName("Button_Sub"));
	m_pBtnSub->addTouchEventListener(this, toucheventselector(QuoteScene::Option));
	m_pTextFieldIns = dynamic_cast<TextField*>(m_csbNode->getChildByName("TextField_Ins"));
	//ListView_Quote
	m_pListView = dynamic_cast<ListView*>(m_csbNode->getChildByName("ListView_Quote"));
	
	//test
	//m_pTxtTmp = dynamic_cast<Text*>(m_csbNode->getChildByName("Text_tmp"));
	//m_pTxtTmp->setTextAreaSize(Size(TXT_W, TXT_H));

	addChild(m_csbNode);

	//自动订阅配置合约
	char *ins[128] = { 0 };
	int count = 0;
	CTP_CFG->GetIns(ins, count);
	CTP_WRAP->Sub(ins, count);

	//创建默认items
	for (int i = 0; i < count; i++)
	{
		QuoteItem * item = new QuoteItem(ins[i]);
		item->Create();
		INS_MANAGER->AddQuoteItem(item);
		m_pListView->pushBackCustomItem(item->GetLayout());
	}
}

void QuoteScene::Option(Object* pSender,TouchEventType type)
{
	switch (type)
	{
	case TOUCH_EVENT_ENDED:
	{
		if (pSender == m_pBtnExit)
		{

			Director::getInstance()->end();
			CTP_WRAP->ReleaseQuote();
			
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			exit(1);
#endif
		}
		else if (pSender == m_pBtnSub)
		{
			char *sz = (char*)m_pTextFieldIns->getString().c_str();
			CTP_WRAP->Sub(sz);
			//添加到队列
			QuoteItem * item = new QuoteItem(sz);
			item->Create();
			INS_MANAGER->AddQuoteItem(item);
			m_pListView->pushBackCustomItem(item->GetLayout());
		}
	}
		break;
	default:
		break;
	}

}

void QuoteScene::OnEvent(IEvent*pEvent)
{
	switch (pEvent->m_id)
	{
		/* CTP */
	case EVENT_CTP_QUOTE_DATA:
	{
		EventQuoteData* pSo = (EventQuoteData*)pEvent;
		//pSo->data.LastPrice
		INS_MANAGER->Update(&pSo->data);

		////test
		//char sz[33];
		//sprintf(sz, "%s", pSo->data.InstrumentID);
		//m_pTxtTmp->setText(sz);
	}
		break;
	case EVENT_CTP_ERR:
	{
		EventCTPErr* pSo = (EventCTPErr*)pEvent;
	}
		break;
	default:
		break;
	}
}
