#ifndef __QuoteScene_SCENE_H__
#define __QuoteScene_SCENE_H__

#include "cocostudio\CocoStudio.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
using namespace cocos2d;
using namespace ui;
#include "./EventProduce/EventProduce.h"

#define USE_TXTBMFONT	1
//#define USE_TXT			1


//ListView ��
class QuoteItem
{
public:
	QuoteItem(char*szIns){ m_pLayout = 0; sprintf(m_szIns, "%s", szIns); }
	void Create();
	void Update(CThostFtdcDepthMarketDataField*pData);
	Layout * GetLayout(){ return m_pLayout; }
private:
	char m_szIns[33];

#if defined(USE_TXT)
	Text * m_pTxtName;
	Text * m_pTxtAsk1;
	Text * m_pTxtBid1;
	Text * m_pTxtAskVol1;
	Text * m_pTxtBidVol1;
#elif defined(USE_TXTBMFONT)
	TextBMFont * m_pTxtName;
	TextBMFont * m_pTxtAsk1;
	TextBMFont* m_pTxtBid1;
	TextBMFont* m_pTxtAskVol1;
	TextBMFont* m_pTxtBidVol1;

#endif
	Layout * m_pLayout;
};

typedef vector<QuoteItem*> vQuoteItems;

class QuoteItemManager
{
public:
	void AddQuoteItem(QuoteItem* pItem){ m_vQuoteItems.push_back(pItem); }
	void Update(CThostFtdcDepthMarketDataField * pData);
private:
	vQuoteItems m_vQuoteItems;
};
#define  INS_MANAGER singleton_t<QuoteItemManager>::instance()


class QuoteScene : public cocos2d::Layer,public IEventListen
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(QuoteScene);

	void Option(Object* pSender,TouchEventType type);

	virtual void onEnterTransitionDidFinish();

public:
	virtual void OnEvent(IEvent*pEvent);
	
private:
	Node *m_csbNode;
	Button * m_pBtnExit;
	Button * m_pBtnSub;
	TextField * m_pTextFieldIns;
	ListView * m_pListView;
private:

	//Text * m_pTxtTmp;
};

#endif // __QuoteScene_SCENE_H__
