#include "FACTPWrapper.h"
#include "../EventProduce/EventProduce.h"


void FAQuote::OnFrontConnected()
{
	IEvent * pEvent = new IEvent(EVENT_CTP_CONNECTED);
	EVENT_PRODUCE->FireEvent(pEvent);

	CTP_WRAP->LoginQuote();
}

void FAQuote::OnFrontDisconnected(int nReason)
{
	IEvent * pEvent = new IEvent(EVENT_CTP_DISCONNECTED);
	EVENT_PRODUCE->FireEvent(pEvent);
}

void FAQuote::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	IEvent * pEvent = new IEvent(EVENT_CTP_QUOTE_LOGIN);
	EVENT_PRODUCE->FireEvent(pEvent);
}

void FAQuote::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	EventCTPErr * pEvent = new EventCTPErr(EVENT_CTP_ERR);
	EVENT_PRODUCE->FireEvent(pEvent);
}

void FAQuote::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pRspInfo && pRspInfo->ErrorID == 0)
	{

	}
}

void FAQuote::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	EventQuoteData * pEvent = new EventQuoteData(EVENT_CTP_QUOTE_DATA);
	memcpy(&pEvent->data, pDepthMarketData, sizeof(CThostFtdcDepthMarketDataField));
	EVENT_PRODUCE->FireEvent(pEvent);
}

void FACTPWrapper::LoginQuote()
{
	CThostFtdcReqUserLoginField stLoginField;
	memset(&stLoginField, 0, sizeof(stLoginField));

	char * ac = CTP_CFG->GetAccount();
	char * broker = CTP_CFG->GetBrokerID();
	char * pwd = CTP_CFG->GetPwd();

	sprintf(stLoginField.BrokerID, "%s", ac);
	sprintf(stLoginField.UserID, "%s", broker);
	sprintf(stLoginField.Password, "%s", pwd);

	m_pQuoteAPI->ReqUserLogin(&stLoginField, ++m_nRequestSeq);
}